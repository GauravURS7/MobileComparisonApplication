<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Page</title>

<style>
tr:first-child {
	font-weight: bold;
	background-color: #C6C9C4;
}
</style>

</head>


<body>

	<h2>List of Products</h2>
	<table>
		<tr>
			<td>ID</td>
			<td>NAME</td>
			<td>Actions</td>
		</tr>
		<c:forEach items="${products}" var="products">
			<tr>
				<td>${products.product_category_id}</td>
				<td>${products.product_category_name}</td>
				<td><a href="<c:url value='/edit-${products.product_category_id}' />">edit</a></td>
				<td><a
					href="<c:url value='/delete-${products.product_category_id}' />">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<br />
	${deleteMessage}
	<br>
	<a href="<c:url value='/new'/>">+ Add New Product Category</a><br><br><br>
	   Go back to <a href="<c:url value='/list' />">List of All Product Category</a>
</body>
</html>