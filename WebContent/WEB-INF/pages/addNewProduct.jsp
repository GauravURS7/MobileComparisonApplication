<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<html>
 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Product Form</title>
 
<style>
 
    .error {
        color: #ff0000;
    }
</style>
 
</head>
 
<body>
 
    <h2>Add New Product Form</h2>
  
    <form:form method="POST" modelAttribute="product">
       
        <table>
            <tr>
                <td><label for="product_category_id">Employee ID : </label> </td>
                <td><form:input path="product_category_id" id="product_category_id"/></td>
                <td><form:errors path="product_category_id" cssClass="error"/></td>
            </tr>
     
            <tr>
                <td><label for="product_category_name">Employee Name: </label> </td>
                <td><form:input path="product_category_name" id="product_category_name"/></td>
                <td><form:errors path="product_category_name" cssClass="error"/></td>
            </tr>
            
            <tr>
                <td colspan="3">
                    <c:choose>
                        <c:when test="${edit}">
                            <input type="submit" value="Update"/>
                        </c:when>
                        <c:otherwise>
                            <input type="submit" value="Save"/>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </form:form>
    <br/>
    <br>
    ${addedMessage}
    <br>
    <br/>
    Go back to <a href="<c:url value='/list' />">List of All Products</a>
</body>
</html>