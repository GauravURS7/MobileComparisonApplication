package com.urs.dto;

import java.io.Serializable;

public class ProductAttributeDTO implements Serializable {

private int productAttributeId;
	
	private String productAttributeName;
	
	private String productAttributeDisplayName;
	
	private String productAttributeValue;

	public int getProductAttributeId() {
		return productAttributeId;
	}

	public void setProductAttributeId(int productAttributeId) {
		this.productAttributeId = productAttributeId;
	}

	public String getProductAttributeName() {
		return productAttributeName;
	}

	public void setProductAttributeName(String productAttributeName) {
		this.productAttributeName = productAttributeName;
	}

	public String getProductAttributeDisplayName() {
		return productAttributeDisplayName;
	}

	public void setProductAttributeDisplayName(String productAttributeDisplayName) {
		this.productAttributeDisplayName = productAttributeDisplayName;
	}

	public String getProductAttributeValue() {
		return productAttributeValue;
	}

	public void setProductAttributeValue(String productAttributeValue) {
		this.productAttributeValue = productAttributeValue;
	}

}
