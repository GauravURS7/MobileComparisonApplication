package com.urs.dto;

import java.io.Serializable;

import com.urs.model.ProductCategory;

public class ProductTypeDTO implements Serializable {
	
private int productTypeId;
	
	private String productTypeName;
	
	private String productTypeDisplayName;
	
	private int productTypePrice;
	
	private byte[] productTypeImage;

	private ProductCategory productCategory;

	public int getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getProductTypeDisplayName() {
		return productTypeDisplayName;
	}

	public void setProductTypeDisplayName(String productTypeDisplayName) {
		this.productTypeDisplayName = productTypeDisplayName;
	}

	public int getProductTypePrice() {
		return productTypePrice;
	}

	public void setProductTypePrice(int productTypePrice) {
		this.productTypePrice = productTypePrice;
	}

	public byte[] getProductTypeImage() {
		return productTypeImage;
	}

	public void setProductTypeImage(byte[] productTypeImage) {
		this.productTypeImage = productTypeImage;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

}
