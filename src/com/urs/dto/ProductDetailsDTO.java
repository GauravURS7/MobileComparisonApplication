package com.urs.dto;

import java.io.Serializable;

import com.urs.model.ProductAttribute;
import com.urs.model.ProductType;

public class ProductDetailsDTO implements Serializable {

	private int productDetailsId;
	
	private ProductType productType;

	private ProductAttribute productAttribute;
	
	public int getProductDetailsId() {
		return productDetailsId;
	}

	public void setProductDetailsId(int productDetailsId) {
		this.productDetailsId = productDetailsId;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public ProductAttribute getProductAttribute() {
		return productAttribute;
	}

	public void setProductAttribute(ProductAttribute productAttribute) {
		this.productAttribute = productAttribute;
	}

}
