package com.urs.dao.ProductDetailsDAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.urs.dao.AbstractDAO;
import com.urs.model.ProductCategory;
import com.urs.model.ProductDetails;
import com.urs.model.ProductType;

@Repository("productDetailsDAOImpl")
@Transactional
public class ProductDetailsDAOImpl extends AbstractDAO<Integer,ProductDetails> implements ProductDetailsDAO {

	
	@Override
	public List<ProductDetails> getAllProductDetailDetails() {
		Criteria criteria = createEntityCriteria();
		return (List<ProductDetails>) criteria.list();
		
	}

	@Override
	public void addNewProductDetails(ProductDetails productDetails) {
		
		persist(productDetails);		
	}

	@Override
	public void deleteProductDetailsById(int productDetailsId) {
		ProductDetails productDetails = getProductDetailsById(productDetailsId);
		delete(productDetails);
		
	}

	@Override
	public void updateProductDetails(ProductDetails productDetails) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProductDetails getProductDetailsById(int productDetailsId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteProductDetailsByProductTypeId(int productTypeId) {
		
	}

	@Override
	public void deleteProductDetailsByProductAttributeId(int productAttributeId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ProductDetails> getProductDetailsByProductType(ProductType pdType) {
		
		Criteria criteria = createEntityCriteria();
		  criteria.add(Restrictions.eq("productType", pdType));
		  List<ProductDetails> pdDetail = (List<ProductDetails>) criteria.list();
		  System.out.println(pdDetail);
		  return pdDetail;
	}

	@Override
	public ProductDetails getProductDetailsByProductAttributeId(int productAttributeId) {
		// TODO Auto-generated method stub
		return null;
	}

}
