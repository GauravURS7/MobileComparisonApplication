package com.urs.dao.ProductDetailsDAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.urs.model.ProductDetails;
import com.urs.model.ProductType;

@Repository
public interface ProductDetailsDAO {
	
	List<ProductDetails> getAllProductDetailDetails();
	void addNewProductDetails(ProductDetails productDetails);
	void deleteProductDetailsById(int productDetailsId);
	void deleteProductDetailsByProductTypeId(int productTypeId);
	void deleteProductDetailsByProductAttributeId(int productAttributeId);
	void updateProductDetails(ProductDetails productDetails);
	ProductDetails getProductDetailsById(int productDetailsId);
	List<ProductDetails> getProductDetailsByProductType(ProductType pdType);
	ProductDetails getProductDetailsByProductAttributeId(int productAttributeId);

}
