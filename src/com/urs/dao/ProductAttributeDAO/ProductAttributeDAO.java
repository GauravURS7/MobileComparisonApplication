package com.urs.dao.ProductAttributeDAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.urs.model.ProductAttribute;

@Repository
public interface ProductAttributeDAO {

	List<ProductAttribute> getAllProductAttributeDetails();
	void addNewProductAttribute(ProductAttribute productAttribute);
	void deleteProductAttributeById(int productAttributeId);
	void updateProductAttribute(ProductAttribute productAttribute);
	ProductAttribute getProductAttributeById(int productAttributeId);
	List<ProductAttribute> getProductAttributeByName(String productAttributeName);
}
