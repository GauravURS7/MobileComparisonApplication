package com.urs.dao.ProductAttributeDAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.urs.dao.AbstractDAO;
import com.urs.model.ProductAttribute;

@Repository("ProductAttributeDAO")
@Transactional
public class ProductAttributeDAOImpl extends AbstractDAO<Integer,ProductAttribute> implements ProductAttributeDAO {

	/*
	 * This method will get every product attribute from database
	 * 
	 * @return list of product attribute object
	 */
	public List<ProductAttribute> getAllProductAttributeDetails() {
		
		Criteria criteria = createEntityCriteria();
		return (List<ProductAttribute>) criteria.list();

	}

	/*
	 * This method will add new product attribute to database
	 * 
	 * @param product attribute object
	 */
	public void addNewProductAttribute(ProductAttribute productAttribute) {
						persist(productAttribute);
	}

	/*
	 * This method will update product attribute in database
	 * 
	 * @param product attribute object
	 */
	public void updateProductAttribute(ProductAttribute productAttribute) {
		
		    update(productAttribute);
	}

	/*
	 * This method will get product attribute by id from database
	 * 
	 * @param product attribute id
	 * 
	 * @return product attribute object
	 */
	public ProductAttribute getProductAttributeById(int productAttributeId) {
		return (ProductAttribute) getByKey(productAttributeId);
	}

	/*
	 * This method will delete product attribute by id from database
	 * 
	 * @param product attribute id
	 */
	public void deleteProductAttributeById(int productAttributeId) {
		ProductAttribute productAttribute = getProductAttributeById(productAttributeId);
		delete(productAttribute);
	}

	/*
	 * This method will delete product attribute by name from database
	 * 
	 * @param product attribute name
	 * 
	 * @return list of product attribute object
	 */
	public List<ProductAttribute> getProductAttributeByName(String productAttributeName) {
		Criteria criteria = createEntityCriteria();
		  criteria.add(Restrictions.ilike("productAttributeName", productAttributeName, MatchMode.ANYWHERE));
		  List<ProductAttribute> pdAttr = (List<ProductAttribute>) criteria.list();
		  return pdAttr;
	}

}
