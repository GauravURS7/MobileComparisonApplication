package com.urs.dao.ProductCategoryDAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.urs.dao.AbstractDAO;
import com.urs.model.ProductCategory;

@Repository("ProductCategoryDAO")
@Transactional
public class ProductCategoryDAOImpl extends AbstractDAO<Integer,ProductCategory> implements ProductCategoryDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	/*
	 * This method will get every product from database
	 */
	public List<ProductCategory> getAllProductCategoryDetails() {
		
		Criteria criteria = createEntityCriteria();
		return (List<ProductCategory>) criteria.list();

	}

	/*
	 * This method will add new product to database
	 */
	public void addNewProductCategory(ProductCategory productCategory) {
						persist(productCategory);
	}

	/*
	 * This method will update product in database
	 */
	public void updateProductCategory(ProductCategory productCategory) {
		
		    update(productCategory);
	}

	/*
	 * This method will get product by id from database
	 */
	public ProductCategory getProductCategoryById(int productCategoryId) {
		return (ProductCategory) getByKey(productCategoryId);
	}

	/*
	 * This method will delete product by id from database
	 */
	public void deleteProductCategoryById(int productCategoryId) {
		ProductCategory productCategory = getProductCategoryById(productCategoryId);
		delete(productCategory);
	}

	/*
	 * This method will get product by name from database
	 */
	public List<ProductCategory> getProductCategoryByName(String productCategoryName) {
		Criteria criteria = createEntityCriteria();
		  criteria.add(Restrictions.ilike("productCategoryName", productCategoryName, MatchMode.ANYWHERE));
		  List<ProductCategory> listProductCategory = (List<ProductCategory>) criteria.list();
		  return listProductCategory;
	}

	
}
