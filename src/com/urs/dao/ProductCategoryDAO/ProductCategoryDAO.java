package com.urs.dao.ProductCategoryDAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.urs.model.ProductCategory;

@Repository
public interface ProductCategoryDAO {

	List<ProductCategory> getAllProductCategoryDetails();
	void addNewProductCategory(ProductCategory productCategory);
	void deleteProductCategoryById(int productCategoryId);
	void updateProductCategory(ProductCategory productCategory);
	ProductCategory getProductCategoryById(int productCategoryId);
	List<ProductCategory> getProductCategoryByName(String productCategoryName);
}
