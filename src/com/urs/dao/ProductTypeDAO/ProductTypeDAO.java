package com.urs.dao.ProductTypeDAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.urs.model.ProductCategory;
import com.urs.model.ProductType;

@Repository
public interface ProductTypeDAO {

	List<ProductType> getAllProductTypeDetails();
	void addNewProductType(ProductType productType);
	void deleteProductTypeById(ProductType productType);
	void updateProductType(ProductType productType);
	ProductType getProductTypeById(int productTypeId);
	List<ProductType> getProductTypeByName(String productTypeName);
}
