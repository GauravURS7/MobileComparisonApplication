package com.urs.dao.ProductTypeDAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.urs.dao.AbstractDAO;
import com.urs.dao.ProductCategoryDAO.ProductCategoryDAO;
import com.urs.dao.ProductDetailsDAO.ProductDetailsDAO;
import com.urs.model.ProductCategory;
import com.urs.model.ProductType;

@Repository("productTypeDAO")
@Transactional
public class ProductTypeDAOImpl extends AbstractDAO<Integer,ProductType> implements ProductTypeDAO {
	
	/*
	 * This method will get every product type from database
	 * 
	 * @return list of ProductType Object
	 */
	public List<ProductType> getAllProductTypeDetails() {
		
		Criteria criteria = createEntityCriteria();
		return (List<ProductType>) criteria.list();

	}

	/*
	 * This method will add new product type to database
	 * 
	 * @param ProductType object
	 */
	public void addNewProductType(ProductType productType) {
		     persist(productType);
	}

	/*
	 * This method will update product type via ProductType object in database
	 * 
	 * @param ProductType object 
	 */
	public void updateProductType(ProductType productType) {
		
		    update(productType);
	}

	/*
	 * This method will get product type by product type id from database
	 * 
	 * @param product type id
	 * 
	 * @return ProductType object
	 */
	public ProductType getProductTypeById(int productTypeId) {
		return (ProductType) getByKey(productTypeId);
	}

	/*
	 * This method will get product type via ProductCategory object from database
	 * 
	 * @param ProductCategory object
	 * 
	 * @return ProductType object
	 */
	public ProductType getProductTypeByProductCategory(ProductCategory productCategory) {
		return (ProductType) getByKey(productCategory.getProductCategoryId());
	}
	
	/*
	 * This method will delete product type by ProductType object from database
	 * 
	 * @param ProductType object
	 */
	public void deleteProductTypeById(ProductType productType) {
		
		delete(productType);
	}

	/*
	 * This method will get ProductType object by product type name from database
	 * 
	 * @param product type name
	 * 
	 * @return list of ProductType object
	 */
	public List<ProductType> getProductTypeByName(String productTypeDisplayName) {
		Criteria criteria = createEntityCriteria();
		  criteria.add(Restrictions.ilike("productTypeDisplayName", productTypeDisplayName, MatchMode.ANYWHERE));
		  List<ProductType> pdType = (List<ProductType>) criteria.list();
		  return pdType;
	}

}
