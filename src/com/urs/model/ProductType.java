package com.urs.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_type")
public class ProductType {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="product_type_id")
	private int productTypeId;
	
	@Column(name="product_type_name")
	private String productTypeName;
	
	@Column(name="product_type_display_name")
	private String productTypeDisplayName;
	
	@Column(name="product_type_price")
	private int productTypePrice;
	
	@Column(name="product_type_image")
	private byte[] productTypeImage;

	@ManyToOne
	@JoinColumn(name = "product_category_id", referencedColumnName = "product_category_id")
	private ProductCategory productCategory;
	
	@OneToMany(mappedBy = "productType", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	 @Fetch(FetchMode.SUBSELECT)
	 @JsonIgnore
	 private List<ProductDetails> productDetails;

	public int getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getProductTypeDisplayName() {
		return productTypeDisplayName;
	}

	public void setProductTypeDisplayName(String productTypeDisplayName) {
		this.productTypeDisplayName = productTypeDisplayName;
	}

	public int getProductTypePrice() {
		return productTypePrice;
	}

	public void setProductTypePrice(int productTypePrice) {
		this.productTypePrice = productTypePrice;
	}

	public byte[] getProductTypeImage() {
		return productTypeImage;
	}

	public void setProductTypeImage(byte[] productTypeImage) {
		this.productTypeImage = productTypeImage;
	}

	public List<ProductDetails> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(List<ProductDetails> productDetails) {
		this.productDetails = productDetails;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	
	/* @ManyToMany(cascade = CascadeType.ALL)
	    @JoinTable(name = "product_details", 
	             joinColumns = { @JoinColumn(name = "product_type_id") }, 
	             inverseJoinColumns = { @JoinColumn(name = "product_attribute_id") })
	 @JsonIgnore
	private ProductAttribute productAttribute;

	public ProductAttribute getProductAttribute() {
		return productAttribute;
	}

	public void setProductAttribute(ProductAttribute productAttribute) {
		this.productAttribute = productAttribute;
	}
*/

	
}
