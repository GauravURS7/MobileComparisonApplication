package com.urs.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="product_attribute")
public class ProductAttribute {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="product_attribute_id")
	private int productAttributeId;
	
	@Column(name="product_attribute_name")
	private String productAttributeName;
	
	@Column(name="product_attribute_display_name")
	private String productAttributeDisplayName;
	
	@Column(name="product_attribute_value")
	private String productAttributeValue;
	
	@OneToMany(mappedBy = "productAttribute", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	 @Fetch(FetchMode.SUBSELECT)
	 @JsonIgnore
	 private List<ProductDetails> productDetails;
	
	public List<ProductDetails> getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(List<ProductDetails> productDetails) {
		this.productDetails = productDetails;
	}
	public int getProductAttributeId() {
		return productAttributeId;
	}
	public void setProductAttributeId(int productAttributeId) {
		this.productAttributeId = productAttributeId;
	}
	public String getProductAttributeName() {
		return productAttributeName;
	}
	public void setProductAttributeName(String productAttributeName) {
		this.productAttributeName = productAttributeName;
	}
	public String getProductAttributeDisplayName() {
		return productAttributeDisplayName;
	}
	public void setProductAttributeDisplayName(String productAttributeDisplayName) {
		this.productAttributeDisplayName = productAttributeDisplayName;
	}
	public String getProductAttributeValue() {
		return productAttributeValue;
	}
	public void setProductAttributeValue(String productAttributeValue) {
		this.productAttributeValue = productAttributeValue;
	}
	
	
}
