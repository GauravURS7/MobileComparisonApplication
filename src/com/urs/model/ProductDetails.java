package com.urs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="product_details")
public class ProductDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="product_details_id")
	private int productDetailsId;
	
	@ManyToOne
	@JoinColumn(name = "product_type_id", referencedColumnName = "product_type_id")
	private ProductType productType;
	
	@ManyToOne
	@JoinColumn(name = "product_attribute_id", referencedColumnName = "product_attribute_id")
	private ProductAttribute productAttribute;
	
	public int getProductDetailsId() {
		return productDetailsId;
	}
	public void setProductDetailsId(int productDetailsId) {
		this.productDetailsId = productDetailsId;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public ProductAttribute getProductAttribute() {
		return productAttribute;
	}
	public void setProductAttribute(ProductAttribute productAttribute) {
		this.productAttribute = productAttribute;
	}

}
