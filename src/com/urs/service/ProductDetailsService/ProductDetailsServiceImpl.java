package com.urs.service.ProductDetailsService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urs.dao.ProductDetailsDAO.ProductDetailsDAO;
import com.urs.dto.ProductDetailsDTO;
import com.urs.model.ProductDetails;
import com.urs.model.ProductType;
import com.urs.service.ProductTypeService.ProductTypeService;

@Service("productDetailsService")
public class ProductDetailsServiceImpl implements ProductDetailsService {

	@Autowired
	ProductDetailsDAO productDetailsDAO;
	
	@Autowired
	ProductTypeService productTypeService;
	
	@Override
	public List<ProductDetailsDTO> getAllProductDetailsDetails() {
		
		List<ProductDetails> listProductDetails = productDetailsDAO.getAllProductDetailDetails();
		
		List<ProductDetailsDTO> listProductDetailsDTO = new ArrayList<ProductDetailsDTO>();
		
		ProductDetailsDTO productDetailsDTO = null ;
		
		for(ProductDetails productDetails : listProductDetails) {
			
			productDetailsDTO = new ProductDetailsDTO();
			productDetailsDTO.setProductDetailsId(productDetails.getProductDetailsId());
			productDetailsDTO.setProductType(productDetails.getProductType());
			productDetailsDTO.setProductAttribute(productDetails.getProductAttribute());
			
			listProductDetailsDTO.add(productDetailsDTO);
		}
		
		return listProductDetailsDTO;
	}

	@Override
	public List<ProductDetailsDTO> addNewProductDetails(ProductDetailsDTO productDetailsDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductDetailsDTO> updateProductDetails(ProductDetailsDTO productDetailsDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductDetailsDTO getProductDetailsById(int productDetailsId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductDetailsDTO> deleteProductDetailsById(int productDetailsId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductDetailsDTO> getProductDetailsByProductType(ProductType productType) {
		
		List<ProductDetails> listProductDetails =  productDetailsDAO.getProductDetailsByProductType(productType);
		
		List<ProductDetailsDTO> listProductDetailsDTO = new ArrayList<ProductDetailsDTO>();
		
		ProductDetailsDTO productDetailsDTO = null ;
		
		for(ProductDetails productDetails : listProductDetails) {
			
			productDetailsDTO = new ProductDetailsDTO();
			productDetailsDTO.setProductDetailsId(productDetails.getProductDetailsId());
			productDetailsDTO.setProductType(productDetails.getProductType());
			productDetailsDTO.setProductAttribute(productDetails.getProductAttribute());
			
			listProductDetailsDTO.add(productDetailsDTO);
		}
		
		return listProductDetailsDTO;
	}

}
