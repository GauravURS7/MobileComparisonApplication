package com.urs.service.ProductDetailsService;

import java.util.List;

import org.springframework.stereotype.Service;

import com.urs.dto.ProductDetailsDTO;
import com.urs.model.ProductType;

@Service
public interface ProductDetailsService {

	List<ProductDetailsDTO> getAllProductDetailsDetails();
	List<ProductDetailsDTO> addNewProductDetails(ProductDetailsDTO productDetailsDTO);
	List<ProductDetailsDTO> updateProductDetails(ProductDetailsDTO productDetailsDTO);
	ProductDetailsDTO getProductDetailsById(int productDetailsId);
	List<ProductDetailsDTO> deleteProductDetailsById(int productDetailsId);
	List<ProductDetailsDTO> getProductDetailsByProductType(ProductType productType);
	
}
