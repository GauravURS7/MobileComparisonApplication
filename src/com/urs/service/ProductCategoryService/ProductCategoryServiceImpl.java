package com.urs.service.ProductCategoryService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urs.dao.ProductCategoryDAO.ProductCategoryDAO;
import com.urs.dto.ProductCategoryDTO;
import com.urs.model.ProductCategory;

@Service("ProductCategoryService")
public class ProductCategoryServiceImpl implements ProductCategoryService {

	@Autowired
	ProductCategoryDAO productCategoryDAO;

	/*
	 * This method will get every product category
	 * 
	 * @return list of ProductCategoryDTO object 
	 */
	public List<ProductCategoryDTO> getAllProductCategoryDetails() {

		List<ProductCategoryDTO> listProductCategoryDTO = null;
		ProductCategoryDTO productCategoryDTO = null;

		List<ProductCategory> listProductCategory = productCategoryDAO.getAllProductCategoryDetails();

		// convert listProductCategory to listProductCategoryDTO
		listProductCategoryDTO = new ArrayList<ProductCategoryDTO>();

		for (ProductCategory productCategory : listProductCategory) {
			productCategoryDTO = new ProductCategoryDTO();

			productCategoryDTO.setProductCategoryId(productCategory.getProductCategoryId());
			productCategoryDTO.setProductCategoryName(productCategory.getProductCategoryName());

			listProductCategoryDTO.add(productCategoryDTO);

		}

		return listProductCategoryDTO;
	}

	/*
	 * This method will add new product category
	 * 
	 * @param ProductCategoryDTO object
	 * 
	 * @return list of ProductCategoryDTO object
	 */
	public List<ProductCategoryDTO> addNewProductCategory(ProductCategoryDTO productCategoryDTO) {

		ProductCategory productCategory = null;

		// convert productCategoryDTO to productCategory
		productCategory = new ProductCategory();
		productCategory.setProductCategoryId(productCategoryDTO.getProductCategoryId());
		productCategory.setProductCategoryName(productCategoryDTO.getProductCategoryName());

		productCategoryDAO.addNewProductCategory(productCategory);

		return getAllProductCategoryDetails();
	}

	/*
	 * This method will update product category via product category id
	 * 
	 * @param ProductCategoryDTO object
	 * @param product category id
	 * 
	 * @return list of ProductCategoryDTO object
	 */
	public List<ProductCategoryDTO> updateProductCategory(ProductCategoryDTO productCategoryDTO,
			int productCategoryId) {

		ProductCategoryDTO prodCategoryDTO = getProductCategoryById(productCategoryId);

		
		if (prodCategoryDTO != null) {

			ProductCategory productCategory = new ProductCategory();			
			productCategory.setProductCategoryName(productCategoryDTO.getProductCategoryName());
			
			productCategoryDAO.updateProductCategory(productCategory);

		}

		return getAllProductCategoryDetails();

	}

	/*
	 * This method will get product category via product category id
	 * 
	 * @param product category id
	 * 
	 * @return ProductCategoryDTO object
	 * 
	 */
	public ProductCategoryDTO getProductCategoryById(int productCategoryId) {
	
		ProductCategoryDTO productCategoryDTO = null;

		ProductCategory productCategory = productCategoryDAO.getProductCategoryById(productCategoryId);

		// convert ProductCategory to ProductCategoryDTO
		productCategoryDTO = new ProductCategoryDTO();

		productCategoryDTO.setProductCategoryId(productCategory.getProductCategoryId());
		productCategoryDTO.setProductCategoryName(productCategory.getProductCategoryName());

		return productCategoryDTO;

	}

	/*
	 * This method will delete product category via product category id
	 * 
	 * @param product category id
	 * 
	 *  @return list of ProductCategoryDTO object
	 */
	public List<ProductCategoryDTO> deleteProductCategoryById(int productCategoryId) {
		productCategoryDAO.deleteProductCategoryById(productCategoryId);
		return getAllProductCategoryDetails();
	}

	/*
	 * This method will get product category via product category name
	 * 
	 * @param product category name
	 * 
	 * @return list of ProductCategoryDTO object
	 */
	public List<ProductCategoryDTO> getProductCategoryByName(String productCategoryName) {

		List<ProductCategoryDTO> listProductCategoryDTO = null;
		ProductCategoryDTO productCategoryDTO = null;

		List<ProductCategory> listProductCategory = productCategoryDAO.getProductCategoryByName(productCategoryName);

		// convert listProductCategory to listProductCategoryDTO
		listProductCategoryDTO = new ArrayList<ProductCategoryDTO>();

		for (ProductCategory productCategory : listProductCategory) {
			productCategoryDTO = new ProductCategoryDTO();

			productCategoryDTO.setProductCategoryId(productCategory.getProductCategoryId());
			productCategoryDTO.setProductCategoryName(productCategory.getProductCategoryName());

			listProductCategoryDTO.add(productCategoryDTO);

		}

		return listProductCategoryDTO;
	}

}
