package com.urs.service.ProductCategoryService;

import java.util.List;

import org.springframework.stereotype.Service;

import com.urs.dto.ProductCategoryDTO;

@Service
public interface ProductCategoryService {

	List<ProductCategoryDTO> getAllProductCategoryDetails();
	List<ProductCategoryDTO> addNewProductCategory(ProductCategoryDTO productCategoryDTO);
	List<ProductCategoryDTO> updateProductCategory(ProductCategoryDTO productCategoryDTO,int productCategoryId);
	ProductCategoryDTO getProductCategoryById(int productCategoryId);
	List<ProductCategoryDTO> deleteProductCategoryById(int productCategoryId);
	List<ProductCategoryDTO> getProductCategoryByName(String productCategoryName);

}
