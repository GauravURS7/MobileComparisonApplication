package com.urs.service.ProductAttributeService;

import java.util.List;

import org.springframework.stereotype.Service;

import com.urs.dto.ProductAttributeDTO;

@Service
public interface ProductAttributeService {

	List<ProductAttributeDTO> getAllProductAttributeDetails();
	List<ProductAttributeDTO> addNewProductAttribute(ProductAttributeDTO productAttributeDTO);
	ProductAttributeDTO getProductAttributeById(int productAttributeId);
	List<ProductAttributeDTO> updateProductAttribute(ProductAttributeDTO productAttributeDTO, int productAttributeId);
	List<ProductAttributeDTO> getProductAttributeByName(String productAttributeName);
	List<ProductAttributeDTO> deleteProductAttributeById(int productAttributeId);

}
