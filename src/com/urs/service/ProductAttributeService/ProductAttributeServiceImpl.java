package com.urs.service.ProductAttributeService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urs.dao.ProductAttributeDAO.ProductAttributeDAO;
import com.urs.dto.ProductAttributeDTO;
import com.urs.model.ProductAttribute;

@Service("ProductAttributeService")
public class ProductAttributeServiceImpl implements ProductAttributeService {

	@Autowired
	ProductAttributeDAO productAttributeDAO;

	/*
	 * This method will get every product attribute from database
	 */
	public List<ProductAttributeDTO> getAllProductAttributeDetails() {

		List<ProductAttributeDTO> listProductAttributeDTO = null;
		ProductAttributeDTO productAttributeDTO = null;

		List<ProductAttribute> listProductAttribute = productAttributeDAO.getAllProductAttributeDetails();

		// convert listProductAttribute to listProductAttributeDTO
		listProductAttributeDTO = new ArrayList<ProductAttributeDTO>();

		for (ProductAttribute productAttribute : listProductAttribute) {
			
			productAttributeDTO = new ProductAttributeDTO();

			productAttributeDTO.setProductAttributeId(productAttribute.getProductAttributeId());
			productAttributeDTO.setProductAttributeName(productAttribute.getProductAttributeName());
			productAttributeDTO.setProductAttributeDisplayName(productAttribute.getProductAttributeDisplayName());
			productAttributeDTO.setProductAttributeValue(productAttribute.getProductAttributeValue());

			listProductAttributeDTO.add(productAttributeDTO);

		}

		return listProductAttributeDTO;
	}

	/*
	 * This method will add new product attribute to database
	 */
	public List<ProductAttributeDTO> addNewProductAttribute(ProductAttributeDTO productAttributeDTO) {
		ProductAttribute productAttribute = null;

		// convert productAttributeDTO to productAttribute
		productAttribute = new ProductAttribute();
		productAttribute.setProductAttributeId(productAttributeDTO.getProductAttributeId());
		productAttribute.setProductAttributeName(productAttributeDTO.getProductAttributeName());
		productAttribute.setProductAttributeDisplayName(productAttributeDTO.getProductAttributeDisplayName());
		productAttribute.setProductAttributeValue(productAttributeDTO.getProductAttributeValue());

		productAttributeDAO.addNewProductAttribute(productAttribute);

		return getAllProductAttributeDetails();
	}

	/*
	 * This method will get product attribute by id from database
	 */
	public ProductAttributeDTO getProductAttributeById(int productAttributeId) {
		
		ProductAttributeDTO productAttributeDTO = null;

		ProductAttribute productAttribute = productAttributeDAO.getProductAttributeById(productAttributeId);

		// convert ProductAttribute to ProductAttributeDTO
		productAttributeDTO = new ProductAttributeDTO();

		productAttributeDTO.setProductAttributeId(productAttribute.getProductAttributeId());
		productAttributeDTO.setProductAttributeName(productAttribute.getProductAttributeName());
		productAttributeDTO.setProductAttributeDisplayName(productAttribute.getProductAttributeDisplayName());
		productAttributeDTO.setProductAttributeValue(productAttribute.getProductAttributeValue());

		return productAttributeDTO;
	}
	
	/*
	 * This method will update product attribute in database
	 */
	public List<ProductAttributeDTO> updateProductAttribute(ProductAttributeDTO productAttributeDTO, int productAttributeId) {
		
		ProductAttributeDTO prodAttributeDTO = getProductAttributeById(productAttributeId);

		if (prodAttributeDTO != null) {

			ProductAttribute productAttribute = new ProductAttribute();
			productAttribute.setProductAttributeId(prodAttributeDTO.getProductAttributeId());
			productAttribute.setProductAttributeName(productAttributeDTO.getProductAttributeName());
			productAttributeDTO.setProductAttributeDisplayName(productAttribute.getProductAttributeDisplayName());
			productAttributeDTO.setProductAttributeValue(productAttribute.getProductAttributeValue());

			productAttributeDAO.updateProductAttribute(productAttribute);

		}

		return getAllProductAttributeDetails();

	}

	/*
	 * This method will delete product attribute by id from database
	 */
	public List<ProductAttributeDTO> deleteProductAttributeById(int productAttributeId) {
		productAttributeDAO.deleteProductAttributeById(productAttributeId);
		return getAllProductAttributeDetails();
	}

	@Override
	public List<ProductAttributeDTO> getProductAttributeByName(String productAttributeName) {
		
		List<ProductAttributeDTO> listProductAttributeDTO = null;
		ProductAttributeDTO productAttributeDTO = null;

		List<ProductAttribute> listProductAttribute = productAttributeDAO.getProductAttributeByName(productAttributeName);

		// convert listProductAttribute to listProductAttributeDTO
		listProductAttributeDTO = new ArrayList<ProductAttributeDTO>();

		for (ProductAttribute productAttribute : listProductAttribute) {
			productAttributeDTO = new ProductAttributeDTO();

			productAttributeDTO.setProductAttributeId(productAttribute.getProductAttributeId());
			productAttributeDTO.setProductAttributeName(productAttribute.getProductAttributeName());
			productAttributeDTO.setProductAttributeDisplayName(productAttribute.getProductAttributeDisplayName());
			productAttributeDTO.setProductAttributeValue(productAttribute.getProductAttributeValue());

			listProductAttributeDTO.add(productAttributeDTO);

		}

		return listProductAttributeDTO;

	}

}
