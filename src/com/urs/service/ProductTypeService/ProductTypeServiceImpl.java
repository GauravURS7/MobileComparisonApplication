package com.urs.service.ProductTypeService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urs.dao.ProductTypeDAO.ProductTypeDAO;
import com.urs.dto.ProductTypeDTO;
import com.urs.model.ProductType;

@Service("productTypeService")
public class ProductTypeServiceImpl implements ProductTypeService {

	@Autowired
	ProductTypeDAO productTypeDAO;

	/*
	 * This method will get every product type 
	 * 
	 * @return list of ProductTypeList object 
	 */
	public List<ProductTypeDTO> getAllProductTypeDetails() {
		
		List<ProductTypeDTO> listProductTypeDTO = null;
		ProductTypeDTO productTypeDTO = null;

		List<ProductType> listProductType = productTypeDAO.getAllProductTypeDetails();

		// convert listProductCategory to listProductCategoryDTO
		listProductTypeDTO = new ArrayList<ProductTypeDTO>();

		for (ProductType productType : listProductType) {
			productTypeDTO = new ProductTypeDTO();

			productTypeDTO.setProductTypeId(productType.getProductTypeId());
			productTypeDTO.setProductTypeName(productType.getProductTypeName());
			productTypeDTO.setProductTypeDisplayName(productType.getProductTypeDisplayName());
			productTypeDTO.setProductTypePrice(productType.getProductTypePrice());
			productTypeDTO.setProductTypeImage(productType.getProductTypeImage());
			productTypeDTO.setProductCategory(productType.getProductCategory());

			listProductTypeDTO.add(productTypeDTO);
		}
		
		return listProductTypeDTO;

	}

	/*
	 * This method will add new product type vai ProductTypeDTO object
	 * 
	 * @param ProductTypeDTO object
	 * 
	 * @return list of ProductTypeDTO object
	 */
	public List<ProductTypeDTO> addNewProductType(ProductTypeDTO productTypeDTO) {
		
		ProductType productType = null;
		
		//converting productTypeDTO into productType
		productType = new ProductType();
		productType.setProductTypeId(productTypeDTO.getProductTypeId());
		productType.setProductTypeName(productTypeDTO.getProductTypeName());
		productType.setProductTypeDisplayName(productTypeDTO.getProductTypeDisplayName());
		productType.setProductTypePrice(productTypeDTO.getProductTypePrice());
		productTypeDTO.setProductTypeImage(productTypeDTO.getProductTypeImage());
		productTypeDTO.setProductCategory(productTypeDTO.getProductCategory());
		
		productTypeDAO.addNewProductType(productType);
		
		return getAllProductTypeDetails();
	}

	/*
	 * This method will update product type
	 * 
	 * @param ProductType object
	 * @param product type id
	 * 
	 * @return ProductTypeDTO object
	 */
	public List<ProductTypeDTO> updateProductType(ProductTypeDTO productTypeDTO, int productTypeId) {
		
		ProductTypeDTO prodTypeDTO = getProductTypeById(productTypeId);

		if (prodTypeDTO != null) {

			ProductType productType = new ProductType();
			productType.setProductTypeName(productTypeDTO.getProductTypeName());
			productType.setProductTypeDisplayName(productTypeDTO.getProductTypeDisplayName());
			productType.setProductTypePrice(productTypeDTO.getProductTypePrice());
			productType.setProductTypeImage(productTypeDTO.getProductTypeImage());
			productType.setProductCategory(productTypeDTO.getProductCategory());
			
			productTypeDAO.updateProductType(productType);

		}
		return getAllProductTypeDetails();
	}

	/*
	 * This method will get product by id from database
	 */
	public ProductTypeDTO getProductTypeById(int productTypeId) {
		ProductTypeDTO productTypeDTO = null;

		ProductType productType = productTypeDAO.getProductTypeById(productTypeId);

		// convert ProductType to ProductTypeDTO
		productTypeDTO = new ProductTypeDTO();

		productTypeDTO.setProductTypeId(productType.getProductTypeId());
		productTypeDTO.setProductTypeName(productType.getProductTypeName());
		productTypeDTO.setProductTypeDisplayName(productType.getProductTypeDisplayName());
		productTypeDTO.setProductTypePrice(productType.getProductTypePrice());
		productTypeDTO.setProductTypeImage(productType.getProductTypeImage());
		productTypeDTO.setProductCategory(productType.getProductCategory());
		
		return productTypeDTO;
		
	}

	/*
	 * This method will delete product by id from database
	 */
	public List<ProductTypeDTO> deleteProductTypeById(int productTypeId) {
	
		ProductTypeDTO productTypeDTO = getProductTypeById(productTypeId);
		
		//convert ProductTypeDTO object to ProductType Bean Object
		ProductType productType = new ProductType();
		productType.setProductTypeId(productTypeDTO.getProductTypeId());
		productType.setProductTypeName(productTypeDTO.getProductTypeName());
		productType.setProductTypeDisplayName(productTypeDTO.getProductTypeDisplayName());
		productType.setProductTypePrice(productTypeDTO.getProductTypePrice());
		productType.setProductTypeImage(productTypeDTO.getProductTypeImage());
		productType.setProductCategory(null);
		
		productTypeDAO.deleteProductTypeById(productType);
		
		return getAllProductTypeDetails();
	}

	/*
	 * This method will get product by name from database
	 */
	public List<ProductTypeDTO> getProductTypeByName(String productTypeName) {
		
		List<ProductType> listProductType = null;
		List<ProductTypeDTO> listProductTypeDTO = new ArrayList<ProductTypeDTO>();
		ProductTypeDTO productTypeDTO = null;
		
		//dao is called to get the list of ProductType entity object
		listProductType =  productTypeDAO.getProductTypeByName(productTypeName);
		
		//converting ProductType object into ProductTypeDTO object
		for(ProductType productType : listProductType) {
			productTypeDTO = new ProductTypeDTO();
			productTypeDTO.setProductTypeId(productType.getProductTypeId());
			productTypeDTO.setProductTypeName(productType.getProductTypeName());
			productTypeDTO.setProductTypeDisplayName(productType.getProductTypeDisplayName());
			productTypeDTO.setProductTypeImage(productType.getProductTypeImage());
			productTypeDTO.setProductTypePrice(productType.getProductTypePrice());
			productTypeDTO.setProductCategory(productType.getProductCategory());
			
			//adding ProductTypeDTO object to List<ProductTypeDTO> list
			listProductTypeDTO.add(productTypeDTO);
		}
		
		return listProductTypeDTO;
	}

}
