package com.urs.service.ProductTypeService;

import java.util.List;

import org.springframework.stereotype.Service;

import com.urs.dto.ProductTypeDTO;
import com.urs.model.ProductType;

@Service
public interface ProductTypeService {

	List<ProductTypeDTO> getAllProductTypeDetails();
	List<ProductTypeDTO> addNewProductType(ProductTypeDTO productTypeDTO);
	List<ProductTypeDTO> updateProductType(ProductTypeDTO productTypeDTO,int productTypeId);
	ProductTypeDTO getProductTypeById(int productTypeId);
	List<ProductTypeDTO> deleteProductTypeById(int productTypeId);
	List<ProductTypeDTO> getProductTypeByName(String productTypeName);
	
}
