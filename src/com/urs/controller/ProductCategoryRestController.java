package com.urs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urs.dto.ProductCategoryDTO;
import com.urs.model.ProductCategory;
import com.urs.service.ProductCategoryService.ProductCategoryService;

@RestController("productCategoryRestController")
public class ProductCategoryRestController {

	@Autowired
	ProductCategoryService productCategoryService; 
	
	/*
	 * This method will list all existing product category.
	 * 
	 * @return list of product category object
	 * 
	 */
	@RequestMapping(value = { "/", "/productCategoryList" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductCategoryDTO>> getProductCategoryDetails() {

		List<ProductCategoryDTO> listProductCategoryDTO = productCategoryService.getAllProductCategoryDetails();

		return new ResponseEntity<List<ProductCategoryDTO>>(listProductCategoryDTO, HttpStatus.OK);
	}//getProductDetails()

	/*
	 * This method will add new product category.
	 * 
	 * @return list of product category object
	 * 
	 */
	@RequestMapping(value = "/addNewProductCategory", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductCategoryDTO>> saveProduct(@RequestBody ProductCategoryDTO productCategoryDTO) {
				
		List<ProductCategoryDTO> listProductCategoryDTO = productCategoryService.addNewProductCategory(productCategoryDTO);
		return new ResponseEntity<List<ProductCategoryDTO>>(listProductCategoryDTO, HttpStatus.OK);
	}

	/*
	 * This method will get product category via product category id.
	 * 
	 * @param id
	 * @param product category object
	 *  
	 * @return list of product category object
	 * 
	 */
	@RequestMapping(value = "/getProductCategory-{productCategoryId}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductCategoryDTO> getProductCategory(@PathVariable int productCategoryId) {
		ProductCategoryDTO productCategoryDTO = productCategoryService.getProductCategoryById(productCategoryId);
		return new ResponseEntity<ProductCategoryDTO>(productCategoryDTO, HttpStatus.OK);
	}

	/*
	 * This method will update product category object.
	 * 
	 * @param product category object
	 * 
	 * @return list of product category object
	 * 
	 */
	@RequestMapping(value = "/updateProductCategory", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductCategoryDTO>> updateProductCategory(@RequestBody ProductCategoryDTO productCategoryDTO) {
				
		List<ProductCategoryDTO> listProductCategoryDTO = productCategoryService.updateProductCategory(productCategoryDTO,productCategoryDTO.getProductCategoryId());
		return new ResponseEntity<List<ProductCategoryDTO>>(listProductCategoryDTO, HttpStatus.OK);

	}

	/*
	 * This method will delete product category by id.
	 * 
	 * @return list of product category object
	 *  
	 */
	@RequestMapping(value = "/deleteProductCategory-{productCategoryId}", method = RequestMethod.DELETE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductCategoryDTO>> deleteProductCategory(@PathVariable int productCategoryId) {
		List<ProductCategoryDTO> listProductCategoryDTO =  productCategoryService.deleteProductCategoryById(productCategoryId);
			return new ResponseEntity<List<ProductCategoryDTO>>(listProductCategoryDTO, HttpStatus.OK);

	}
	
	/*
	 * This method will search product category by name.
	 * 
	 * @return list of product category object
	 *  
	 */
	@RequestMapping(value = "/searchProductCategoryByName-{productCategoryName}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductCategoryDTO>> searchProductCategoryByName(@PathVariable String productCategoryName) {
		List<ProductCategoryDTO> listProductCategoryDTO = productCategoryService.getProductCategoryByName(productCategoryName);
		return new ResponseEntity<List<ProductCategoryDTO>>(listProductCategoryDTO, HttpStatus.OK);
	}
}
