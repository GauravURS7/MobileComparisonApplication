package com.urs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urs.dto.ProductAttributeDTO;
import com.urs.service.ProductAttributeService.ProductAttributeService;

@RestController("productAttributeRestController")
public class ProductAttributeRestController {


	@Autowired
	ProductAttributeService productAttributeService;
	
	/*
	 * This method will list all existing product attribute.
	 * 
	 * @return list product attribute object
	 * 
	 */
	@RequestMapping(value = "/productAttributeList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductAttributeDTO>> getProductDetails() {

		List<ProductAttributeDTO> productAttributeDTO = productAttributeService.getAllProductAttributeDetails();

		return new ResponseEntity<List<ProductAttributeDTO>>(productAttributeDTO, HttpStatus.OK);
	}//getProductDetails()

	/*
	 * This method will add new product attribute object.
	 * 
	 * @param product attribute object
	 * 
	 * @return list product attribute object
	 * 
	 */
	@RequestMapping(value = "/addNewProductAttribute", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductAttributeDTO>> saveProduct(@RequestBody ProductAttributeDTO productAttributeDTO) {
	
		List<ProductAttributeDTO> listProductAttributeDTO = productAttributeService.addNewProductAttribute(productAttributeDTO);
		return new ResponseEntity<List<ProductAttributeDTO>>(listProductAttributeDTO, HttpStatus.OK);

	}

	/*
	 * This method will find product attribute object via product attribute id.
	 * 
	 * @param product attribute id
	 * 
	 * @return list product attribute object
	 *  
	 */
	@RequestMapping(value = "/getProductAttribute-{productAttributeId}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductAttributeDTO> getProductAttribute(@PathVariable int productAttributeId) {
		ProductAttributeDTO productAttributeDTO = productAttributeService.getProductAttributeById(productAttributeId);
		return new ResponseEntity<ProductAttributeDTO>(productAttributeDTO, HttpStatus.OK);
	}

	/*
	 * This method will update old product attribute object with new product attribute object .
	 * 
	 * @param product attribute object
	 * 
	 * @return list product attribute object
	 *  
	 */
	@RequestMapping(value = "/updateProductAttribute", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductAttributeDTO>> updateProductAttribute(@RequestBody ProductAttributeDTO productAttributeDTO) {
				
		List<ProductAttributeDTO> listProductAttributeDTO = productAttributeService.updateProductAttribute(productAttributeDTO,productAttributeDTO.getProductAttributeId());
		return new ResponseEntity<List<ProductAttributeDTO>>(listProductAttributeDTO, HttpStatus.OK);

	}

	/*
	 * This method will delete product attribute object via  product attribute id.
	 * 
	 * @param product attribute id
	 * 
	 * @return list product attribute object 
	 * 
	 */
	@RequestMapping(value = "/deleteProductAttribute-{productAttributeId}", method = RequestMethod.DELETE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductAttributeDTO>> deleteProductAttribute(@PathVariable int productAttributeId) {
		List<ProductAttributeDTO> listProductAttributeDTO =  productAttributeService.deleteProductAttributeById(productAttributeId);
		return new ResponseEntity<List<ProductAttributeDTO>>(listProductAttributeDTO, HttpStatus.OK);

	}

	/*
	 * This method will search product attribute object via product attribute name.
	 * 
	 * @param product attribute name
	 * 
	 * @return list product attribute object
	 *  
	 */
	@RequestMapping(value = "/searchProductAttributeByName-{productAttributeName}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductAttributeDTO>> searchProductAttributeByName(@PathVariable String productAttributeName) {
		List<ProductAttributeDTO> productAttributeDTO = productAttributeService.getProductAttributeByName(productAttributeName);
		return new ResponseEntity<List<ProductAttributeDTO>>(productAttributeDTO, HttpStatus.OK);
	}
}
