package com.urs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urs.dto.ProductDetailsDTO;
import com.urs.dto.ProductTypeDTO;
import com.urs.model.ProductDetails;
import com.urs.model.ProductType;
import com.urs.service.ProductDetailsService.ProductDetailsService;
import com.urs.service.ProductTypeService.ProductTypeService;

@RestController
public class ProductDetailsRestController {

	@Autowired
	ProductDetailsService productDetailsService;
	
	@Autowired
	ProductTypeService productTypeService;
	
	/*
	 * This method will list all existing products.
	 */
	@RequestMapping(value = "/productDetailsList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductDetailsDTO>> getProductDetailsDetails() {

		List<ProductDetailsDTO> productDetailsDTO = productDetailsService.getAllProductDetailsDetails();

		return new ResponseEntity<List<ProductDetailsDTO>>(productDetailsDTO, HttpStatus.OK);
	}//getProductDetailsDetails()
	
	/*
	 * This method will add new product.
	 */
	@RequestMapping(value = "/addNewProductDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE/*, produces = MediaType.APPLICATION_JSON_VALUE*/)
	public ResponseEntity<List<ProductDetailsDTO>> saveProduct(@RequestBody ProductDetails productDetails) {
		
		return null;
	}

	/*
	 * This method will get product via product detail id.
	 */
	@RequestMapping(value = "/getProductDetails-{productDetailsId}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductDetailsDTO> getProductDetails(@PathVariable int productDetailsId, ModelMap model) {
		
		return null;
	}

	/*
	 * This method will update product via product detail object.
	 */
	@RequestMapping(value = "/updateProductDetails", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE/*, produces = MediaType.APPLICATION_JSON_VALUE*/)
	public ResponseEntity<List<ProductDetailsDTO>> updateProductDetails(@RequestBody ProductDetails productDetails) {
		
		return null;

	}

	/*
	 * This method will delete product by id.
	 */
	@RequestMapping(value = "/deleteProductDetails-{productDetailsId}", method = RequestMethod.DELETE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductDetailsDTO>> deleteProductDetails(@PathVariable int productDetailsId) {
		
			return null;

	}
	
	/*
	 * This method will get product by product type id.
	 */
	@RequestMapping(value = "/searchProductDetailsByProductTypeId-{productTypeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductDetailsDTO>> searchProductDetailsByProductTypeId(@PathVariable int productTypeId) {
		
		ProductTypeDTO productTypeDTO = productTypeService.getProductTypeById(productTypeId);
		
		ProductType productType = new ProductType();
		productType.setProductTypeId(productTypeDTO.getProductTypeId());
		productType.setProductTypeName(productTypeDTO.getProductTypeName());
		productType.setProductTypeDisplayName(productTypeDTO.getProductTypeDisplayName());
		productType.setProductTypePrice(productTypeDTO.getProductTypePrice());
		productType.setProductTypeImage(productTypeDTO.getProductTypeImage());
		productType.setProductCategory(productTypeDTO.getProductCategory());
		
		List<ProductDetailsDTO> productDetailsDTO = productDetailsService.getProductDetailsByProductType(productType);
		return new ResponseEntity<List<ProductDetailsDTO>>(productDetailsDTO, HttpStatus.OK);

	}
	
}
