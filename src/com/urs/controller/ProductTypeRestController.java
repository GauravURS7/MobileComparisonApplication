package com.urs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urs.dto.ProductCategoryDTO;
import com.urs.dto.ProductTypeDTO;
import com.urs.model.ProductCategory;
import com.urs.model.ProductType;
import com.urs.service.ProductCategoryService.ProductCategoryService;
import com.urs.service.ProductTypeService.ProductTypeService;

@RestController("productTypeRestController")
public class ProductTypeRestController {

	@Autowired
	ProductTypeService productTypeService;

	@Autowired
	ProductCategoryService productCategoryService;

	/*
	 * This method will list all existing product type.
	 * 
	 * @return list ProductTypeDTO object
	 */
	@RequestMapping(value = "/productTypeList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductTypeDTO>> getProductTypeDetails() {

		//service is called to get the list of productTypeDTO object
		List<ProductTypeDTO> productTypeDTO = productTypeService.getAllProductTypeDetails();

		return new ResponseEntity<List<ProductTypeDTO>>(productTypeDTO, HttpStatus.OK);
	}// getProductTypeDetails()

	/*
	 * This method will add new product type.
	 * 
	 * @param ProductTypeVO object
	 * 
	 * @return list of ProductTypeDTO object
	 */
	@RequestMapping(value = "/addNewProductType", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductTypeDTO>> saveProduct(@RequestBody ProductTypeDTO productTypeDTO) {
		
		ProductCategoryDTO productCategoryDTO = null;
		ProductCategory productCategory = null;
		
		//get the ProductCategory object via product category id
		int productCategoryId = productTypeDTO.getProductCategory().getProductCategoryId();
		
		//service is called to get the ProductCategoryDTO object via product category id
		productCategoryDTO = productCategoryService.getProductCategoryById(productCategoryId);
		
		//set the ProductCategory Object
		productCategory = new ProductCategory();
		productCategory.setProductCategoryId(productCategoryDTO.getProductCategoryId());
		productCategory.setProductCategoryName(productCategoryDTO.getProductCategoryName());
		
		//set the ProductCategory object To ProductTypeDTO object 
		productTypeDTO.setProductCategory(productCategory);
		
		//service is called to get list of ProductTypeDTO object and add the new product type into database
		List<ProductTypeDTO> listProductTypeDTO = productTypeService.addNewProductType(productTypeDTO);
		
		return new ResponseEntity<List<ProductTypeDTO>>(listProductTypeDTO, HttpStatus.OK);
	}

	/*
	 * This method will get product type.
	 * 
	 * @param product type id
	 * 
	 * @return ProductTypeDTO object
	 */
	@RequestMapping(value = "/getProductType-{productTypeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductTypeDTO> getProductType(@PathVariable int productTypeId) {
		
		//service is called to get the ProductTypeDTO object via product type id
		ProductTypeDTO productTypeDTO = productTypeService.getProductTypeById(productTypeId);
		
		return new ResponseEntity<ProductTypeDTO>(productTypeDTO, HttpStatus.OK);
	}

	/*
	 * This method will update product type via ProductType object.
	 * 
	 * @param ProductType object
	 * 
	 * @return list of ProductType object
	 */
	@RequestMapping(value = "/updateProductType", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductTypeDTO>> updateProductType(@RequestBody ProductTypeDTO productTypeDTO) {
		
		ProductCategoryDTO productCategoryDTO = null;
		ProductCategory productCategory = null;
		
		//get the ProductCategory object via product category id
		int productCategoryId = productTypeDTO.getProductCategory().getProductCategoryId();
		
		//service is called to get ProductCategoryDTO object via product category id
		productCategoryDTO = productCategoryService.getProductCategoryById(productCategoryId);
		
		//get the ProductCategory
		productCategory = new ProductCategory();
		productCategory.setProductCategoryId(productCategoryDTO.getProductCategoryId());
		productCategory.setProductCategoryName(productCategoryDTO.getProductCategoryName());
		
		//set the ProductCategory object to ProductTypeDTO object
		productTypeDTO.setProductCategory(productCategory);		
		
		//service is called to update the ProductType object into database and get list of ProductTypeDTO object
		List<ProductTypeDTO> listProductTypeDTO = productTypeService.updateProductType(productTypeDTO,productTypeDTO.getProductTypeId());

		return new ResponseEntity<List<ProductTypeDTO>>(listProductTypeDTO, HttpStatus.OK);

	}

	/*
	 * This method will delete product type by product type id.
	 * 
	 * @param product type id
	 * 
	 * @return list of ProductType
	 */
	@RequestMapping(value = "/deleteProductType-{productTypeId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductTypeDTO>> deleteProductType(@PathVariable int productTypeId) {
		
		//service is called to delete the ProductType object via product type id and get list of ProductTypeDTO object  
		List<ProductTypeDTO> productTypeDTO = productTypeService.deleteProductTypeById(productTypeId);

		return new ResponseEntity<List<ProductTypeDTO>>(productTypeDTO, HttpStatus.OK);

	}

	/*
	 * This method will search product type by product type name.
	 * 
	 * @param 
	 */
	@RequestMapping(value = "/searchProductTypeByName-{productTypeName}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductTypeDTO>> searchProductCategoryByName(@PathVariable String productTypeName) {
		
		//service is called to get ProductTypeDTO object via product type name and get list of ProductType object
		List<ProductTypeDTO> productTypeDTO = productTypeService.getProductTypeByName(productTypeName);
		
		return new ResponseEntity<List<ProductTypeDTO>>(productTypeDTO, HttpStatus.OK);
	}
}
