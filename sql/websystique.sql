/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.48-log : Database - websystique
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`websystique` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `websystique`;

/*Table structure for table `product_attribute` */

DROP TABLE IF EXISTS `product_attribute`;

CREATE TABLE `product_attribute` (
  `product_attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_attribute_display_name` varchar(255) DEFAULT NULL,
  `product_attribute_name` varchar(255) DEFAULT NULL,
  `product_attribute_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `product_attribute` */

insert  into `product_attribute`(`product_attribute_id`,`product_attribute_display_name`,`product_attribute_name`,`product_attribute_value`) values (1,'Display','Summary_Xiaomi_Display','5.0\" (Medium)'),(2,'Performance','Summary_Xiaomi_Performance','Octa Core, 1.4 GHz, 2 GB RAM'),(3,'Camera','Summary_Xiaomi_Camera','13 MP'),(4,'Operating System','General_Xiaomi_OS','Android v6.0 (Marshmallow)'),(5,'Network','General_Xiaomi_Network','4G : Yes, 3G : Yes, 2G : Yes'),(6,'Internal Memory','Storage_Xiaomi_IM','16 GB'),(7,'Expandable Memory','Storage_Xiaomi_EM','upto 128 GB'),(8,'Display','Summary_Moto_Display','5.0\"'),(9,'Performance','Summary_Moto_Performance','Quad Core, 1.2 GHz, 2 GB RAM'),(10,'Camera','Summary_Moto_Camera','8 MP'),(11,'Operating System','General_Moto_OS','Android v6.0.1 (Marshmallow)'),(12,'Network','General_Moto_Network','4G : Yes, 3G : Yes, 2G : Yes'),(13,'Internal Memory','General_Moto_IM','16 GB'),(14,'Expandable Memory','General_Moto_EM','64 GB'),(15,'Display','Summary_Samsung_Display','5.5\"'),(16,'Performance','Summary_Samsung_Performance','Quad core, 1.3 GHz, 2 GB RAM'),(17,'Camera','Summary_Samsung_Camera','5 MP'),(18,'Operating System','General_Samsung_OS','Android v5.0 (Lollipop)'),(19,'Network','General_Samsung_Network','4G : Yes, 3G : Yes, 2G : Yes'),(20,'Internal Memory','Storage_Samsung_IM','8 GB'),(21,'Expandable Memory','General_Samsung_EM','32 GB');

/*Table structure for table `product_category` */

DROP TABLE IF EXISTS `product_category`;

CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `product_category` */

insert  into `product_category`(`product_category_id`,`product_category_name`) values (1,'mobiles'),(2,'TV'),(3,'Furniture'),(4,'watch');

/*Table structure for table `product_details` */

DROP TABLE IF EXISTS `product_details`;

CREATE TABLE `product_details` (
  `product_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_attribute_id` int(11) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_details_id`),
  KEY `FKay2ouu1eprir6moh618ilaj4v` (`product_attribute_id`),
  KEY `FKfcauwtscdysgbo8u4u98w10x4` (`product_type_id`),
  CONSTRAINT `FKay2ouu1eprir6moh618ilaj4v` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attribute` (`product_attribute_id`),
  CONSTRAINT `FKfcauwtscdysgbo8u4u98w10x4` FOREIGN KEY (`product_type_id`) REFERENCES `product_type` (`product_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `product_details` */

insert  into `product_details`(`product_details_id`,`product_attribute_id`,`product_type_id`) values (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,5,1),(6,6,1),(7,7,1),(8,8,2),(9,9,2),(10,10,2),(11,11,2),(12,12,2),(13,13,2),(14,14,2),(15,15,3),(16,16,3),(17,17,3),(18,18,3),(19,19,3),(20,20,3),(21,21,3);

/*Table structure for table `product_type` */

DROP TABLE IF EXISTS `product_type`;

CREATE TABLE `product_type` (
  `product_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_display_name` varchar(255) DEFAULT NULL,
  `product_type_name` varchar(255) DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `product_type_price` int(11) NOT NULL,
  `product_type_image` tinyblob,
  PRIMARY KEY (`product_type_id`),
  KEY `FK1kf0jqpi79x9p0wwa4fsah38i` (`product_category_id`),
  CONSTRAINT `FK1kf0jqpi79x9p0wwa4fsah38i` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`product_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `product_type` */

insert  into `product_type`(`product_type_id`,`product_type_display_name`,`product_type_name`,`product_category_id`,`product_type_price`,`product_type_image`) values (1,'Xiaomi Redmi Note 3','Xiaomi',1,9999,NULL),(2,'Moto G3 Turbo XTRA','Moto',1,8599,NULL),(3,'Samsung Galaxy Pro','Samsung',1,7990,NULL),(4,'Xiaomi Redmi Note 4','Xiaomi',1,14599,NULL),(5,'Xiaomi Max Prime','Xiaomi',1,19999,NULL),(6,'Moto E3 Power','Moto',1,8099,NULL),(7,'Samsung Galaxy J7 Prime','Samsung',1,15900,NULL),(8,'Asus Zenfone 3 Max','Asus',1,12499,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
